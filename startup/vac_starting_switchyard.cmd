# @field PLC
# @type STRING
# Name of the PLC (SYS-SUBSYS:DIS-DEV-IDX)

# @field GAUGE_1
# @type STRING
# Name of the first gauge (SYS-SUBSYS:DIS-DEV-IDX)

# @field GAUGE_2
# @type STRING
# Name of the second gauge (SYS-SUBSYS:DIS-DEV-IDX)

# @field VPG
# @type STRING
# Name of the vacuum pumping group (SYS-SUBSYS:DIS-DEV-IDX)

dbLoadRecords("vac_starting_switchyard.db", "GAUGE_1 = $(GAUGE_1), GAUGE_2 = $(GAUGE_2), VPG = $(VPG), PLC = $(PLC)")
